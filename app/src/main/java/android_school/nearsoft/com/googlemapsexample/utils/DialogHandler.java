package android_school.nearsoft.com.googlemapsexample.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by emmanuel on 26/08/15.
 */
public class DialogHandler {
    private ProgressDialog progressDialog;
    private Context context;

    public DialogHandler(Context context) {
        this.context = context;
    }

    public void showProgressDialog(String text) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
        }
        progressDialog.setCancelable(false);
        progressDialog.setMessage(text.isEmpty() ? "Por favor, espere..." : text);
        progressDialog.show();

    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showAlertDialog(String titulo, String mensaje) {
        hideProgressDialog();

        new AlertDialog.Builder(context)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setNeutralButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


}
