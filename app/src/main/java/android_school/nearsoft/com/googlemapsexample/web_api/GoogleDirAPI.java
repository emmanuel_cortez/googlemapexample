package android_school.nearsoft.com.googlemapsexample.web_api;


import android_school.nearsoft.com.googlemapsexample.models.DirectionResults;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by emmanuel on 21/07/15.
 */
public interface GoogleDirAPI {
    public static String MODE_DRIVING = "driving";
    public static String MODE_WALKING = "walking";

    public static final String base_url = "https://maps.googleapis.com/";

    //Version asincrona de las llamadas
    @GET("/maps/api/directions/json")
    public void obtenerCamino(@Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode, Callback<DirectionResults> callback);

    @GET("/maps/api/directions/json")
    public void obtenerCamino(@Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode, @Query("waypoints") String waypoints, Callback<DirectionResults> callback);

}
