package android_school.nearsoft.com.googlemapsexample;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import android_school.nearsoft.com.googlemapsexample.models.DirectionResults;
import android_school.nearsoft.com.googlemapsexample.utils.DialogHandler;
import android_school.nearsoft.com.googlemapsexample.utils.Utils;
import android_school.nearsoft.com.googlemapsexample.web_api.GoogleDirAPI;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GoogleDirAPI servicio;
    private DialogHandler dialogHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(GoogleDirAPI.base_url)
                .build();

        servicio = restAdapter.create(GoogleDirAPI.class);

        dialogHandler = new DialogHandler(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        //mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        LatLng laXJuarez = new LatLng(31.7561635, -106.4414129);
        LatLng nearsoftChihuas = new LatLng(28.6320679, -106.0717847);

        mMap.addMarker(new MarkerOptions().position(laXJuarez).title("La X, Juaritoz"));
        mMap.addMarker(new MarkerOptions().position(nearsoftChihuas).title("Nearsoft Chihuahua"));

        buscarDireccionPorTexto(nearsoftChihuas, laXJuarez);
    }

    private void buscarDireccionPorTexto(final LatLng origen, final LatLng destino) {

        dialogHandler.showProgressDialog("");

        servicio.obtenerCamino(origen.latitude + "," + origen.longitude,
                destino.latitude + "," + destino.longitude,
                //GoogleDirAPI.MODE_WALKING,
                GoogleDirAPI.MODE_DRIVING,
                new Callback<DirectionResults>() {
                    @Override
                    public void success(DirectionResults directionResults, Response response) {
                        procesarResultado(directionResults);

                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        builder.include(origen);
                        builder.include(destino);
                        LatLngBounds bounds = builder.build();

                        int padding = 10;
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                        dialogHandler.hideProgressDialog();
                        mMap.moveCamera(cu);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.println(Log.ERROR, "RetrofitError", error.getMessage());
                    }
                });
    }

    private void procesarResultado(DirectionResults directionResults) {
        //Este camino se va a guardar en la "memoria del mapa"
        ArrayList<LatLng> camino = new ArrayList<LatLng>();

        //Si hay al menos una ruta en el resultado
        if (directionResults.getRoutes().size() > 0) {
            //Lista en que se van a guardar los puntos decodificatos del camino
            ArrayList<LatLng> listaDecodif;

            //Se usara la primera ruta del resultado (es la mas rapida)
            DirectionResults.Route ruta1 = directionResults.getRoutes().get(0);

            //se obtienen los fragmentos de la ruta
            if (ruta1.getLegs().size() > 0) {
                //Se obtienen los "pasos" a segir para dibujar cada fragmento
                List<DirectionResults.Steps> listaPasos = ruta1.getLegs().get(0).getSteps();
                DirectionResults.Steps paso;

                //Localizacion auxiliar
                DirectionResults.Location loc;

                //Polilinea codificada con los puntos a graficar
                String polyline;
                //Para cada paso de la lista de pasos
                for (DirectionResults.Steps step : listaPasos) {
                    paso = step;

                    //Se introduce el primer punto
                    loc = paso.getStart_location();
                    camino.add(new LatLng(loc.getLat(), loc.getLng()));

                    //Se decodifican los puntos intermedios y se agregan al camino
                    polyline = paso.getPolyline().getPoints();
                    listaDecodif = Utils.decodePoly(polyline);
                    camino.addAll(listaDecodif);

                    //se introduce el ultimo punto
                    loc = paso.getEnd_location();
                    camino.add(new LatLng(loc.getLat(), loc.getLng()));
                }

                if (camino.size() > 0) {
                    PolylineOptions rectLine = new PolylineOptions().width(10).color(Color.RED);

                    for (int i = 0; i < camino.size(); i++) {
                        rectLine.add(camino.get(i));
                    }
                    // Adding route on the map
                    mMap.addPolyline(rectLine);
                }
            }
        }

    }

}
